import { controls } from '../../constants/controls';

const CRIT_DELAY = 10;

// Math helpers
const zeroOrMore = (value: number) => Math.max(value, 0);
const percentOf = (total: number, value: number) => Math.round((100 / total) * value);
const getChance = () => Math.random() + 1;

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const pressedKeyCodes = new Set();

  const state: IState = {
    hasBlock: new WeakMap(),
    currentHealth: new WeakMap(),
    totalHealth: new WeakMap(),
    indicatorElement: new WeakMap(),
    lastCritTime: new WeakMap(),
  };

  const initState = (fighter: IFighterDetails, indicatorElementId: string) => {
    const { health } = fighter;
    const indicatorElement = document.getElementById(indicatorElementId);
    if (!indicatorElement) {
      throw new Error('Cannot init state');
    }
    state.hasBlock.set(fighter, false);
    state.currentHealth.set(fighter, health);
    state.totalHealth.set(fighter, health);
    state.indicatorElement.set(fighter, indicatorElement);
  };

  initState(firstFighter, 'left-fighter-indicator');
  initState(secondFighter, 'right-fighter-indicator');

  const updateIndicator = (fighter: IFighterDetails): void => {
    const element = state.indicatorElement.get(fighter);
    if (!element) {
      return;
    }
    const totalHealth = state.totalHealth.get(fighter);
    const currentHealth = state.currentHealth.get(fighter);
    if (totalHealth && currentHealth) {
      const percent = percentOf(totalHealth, zeroOrMore(currentHealth));
      element.setAttribute('style', `width: ${percent}%`);
      return;
    }
    throw new Error('Cannot update indicator');
  };

  const decrementHealth = (fighter: IFighterDetails, damage: number) => {
    const oldValue = state.currentHealth.get(fighter);
    if (!oldValue) {
      throw new Error('Cannot decrement health');
    }
    const newValue = oldValue - damage;
    state.currentHealth.set(fighter, newValue);
    return newValue;
  };

  const calcDamage = (attacker: IFighterDetails, defender: IFighterDetails, isCriticalHit = false) => {
    if (isCriticalHit) {
      return getHitPower(attacker) * 2;
    }
    if (state.hasBlock.get(defender)) {
      return 0;
    }
    return getDamage(attacker, defender);
  };

  const isCriticalHitReady = (fighter: IFighterDetails) => {
    const last = state.lastCritTime.get(fighter);
    if (!last) {
      return true;
    }
    const diff = new Date().getTime() - last.getTime();
    return (diff / 1000) > CRIT_DELAY;
  };

  return new Promise((resolve: (value: IFighterDetails) => void) => {
    const attack = (attacker: IFighterDetails, defender: IFighterDetails, withCriticalHitCombination = false) => {
      if (withCriticalHitCombination && !isCriticalHitReady(attacker)) {
        return;
      }
      const isCriticalHit = withCriticalHitCombination && isCriticalHitReady(attacker);
      const damage = calcDamage(attacker, defender, isCriticalHit);
      const health = decrementHealth(defender, damage);
      updateIndicator(defender);

      if (isCriticalHit) {
        state.lastCritTime.set(attacker, new Date());
      }

      // Victory case
      if (health < 0) {
        document.removeEventListener('keydown', onKeyDown, true);
        document.removeEventListener('keyup', onKeyUp, true);
        resolve(attacker);
      }
    };

    const onKeyDown = (event: KeyboardEvent) => {
      const { code } = event;

      const { PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock } = controls;

      switch (code) {
        case PlayerOneAttack:
          // Player should release button before attack again
          if (pressedKeyCodes.has(code)) {
            break;
          }

          pressedKeyCodes.add(code);

          // Player can't attack when block is set
          if (pressedKeyCodes.has(PlayerOneBlock)) {
            break;
          }

          attack(firstFighter, secondFighter);
          break;

        case PlayerTwoAttack:
          // Player should release button before attack again
          if (pressedKeyCodes.has(code)) {
            break;
          }

          pressedKeyCodes.add(code);

          // Player can't attack when block is set
          if (pressedKeyCodes.has(PlayerTwoBlock)) {
            break;
          }

          attack(secondFighter, firstFighter);
          break;

        case PlayerOneBlock:
          state.hasBlock.set(firstFighter, true);
          break;

        case PlayerTwoBlock:
          state.hasBlock.set(secondFighter, true);
          break;
      }

      pressedKeyCodes.add(code);

      const checkCritCombination = (keys: string[]) => keys.every((key: string) => pressedKeyCodes.has(key));

      if (checkCritCombination(controls.PlayerOneCriticalHitCombination)) {
        attack(firstFighter, secondFighter, true);
      }
      if (checkCritCombination(controls.PlayerTwoCriticalHitCombination)) {
        attack(secondFighter, firstFighter, true);
      }
    };

    const onKeyUp = (event: KeyboardEvent) => {
      const { code } = event;
      const { PlayerOneBlock, PlayerTwoBlock } = controls;

      pressedKeyCodes.delete(code);

      switch (code) {
        case PlayerOneBlock:
          state.hasBlock.set(firstFighter, false);
          break;

        case PlayerTwoBlock:
          state.hasBlock.set(secondFighter, false);
          break;
      }
    };

    document.addEventListener('keydown', onKeyDown, true);
    document.addEventListener('keyup', onKeyUp, true);
  });
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return zeroOrMore(damage);
}

export function getHitPower(fighter: IFighterDetails) {
  const { attack } = fighter;
  const criticalHitChance = getChance();
  return attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighterDetails) {
  const { defense } = fighter;
  const dodgeChance = getChance();
  return defense * dodgeChance;
}
