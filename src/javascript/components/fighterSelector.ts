import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';

export function createFightersSelector(): IFighterSelector {
  let selectedFighters: FighterPair = [];

  return async (event: Event, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    if (!fighter) {
      return;
    }
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = <FighterCache> new Map();

/**
 * Get fighter info by id. Returns cached data from the `fighterDetailsMap`, or get it from `FighterService`.
 * @param {string} fighterId
 */
export async function getFighterInfo(fighterId: string) {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId);
  }
  const fighterDetails = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterDetails);
  return fighterDetails;
}

function renderSelectedFighters(selectedFighters: FighterPair) {
  const fightersPreview = document.querySelector('.preview-container___root');

  if (!fightersPreview) {
    return;
  }

  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FighterPair) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FighterPair) {
  const [first, second] = selectedFighters;
  
  if (!first || !second) {
    throw new Error('Cannot start fight');
  }
  
  renderArena([first, second]);
}
