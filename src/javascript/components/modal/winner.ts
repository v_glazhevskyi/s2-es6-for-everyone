import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter: IFighter) {
  const { name } = fighter;

  const image = createFighterImage(fighter);

  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal-body__winner',
  });

  bodyElement.append(image);

  showModal({
    title: `${name} is won!`.toUpperCase(),
    bodyElement,
    onClose: () => {
      const root = document.getElementById('root');
      
      if (!root) {
        throw new Error('Cannot find root element');
      }
      
      root.innerHTML = '';
      new App();
    },
  });
}
