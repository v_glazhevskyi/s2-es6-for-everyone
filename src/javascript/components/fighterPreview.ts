import { createElement, appendTextNode, createList } from '../helpers/domHelper';

export function createFighterPreview(fighter: MaybeFighter, position: 'left' | 'right') {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    const chooseFighterTitle = createChooseFighterTitle();
    fighterElement.append(chooseFighterTitle);
    return fighterElement;
  }

  const fighterImageElement = createFighterImage(fighter);
  const fighterInfoElement = createFighterInfo(fighter);

  fighterElement.append(fighterImageElement);
  fighterElement.append(fighterInfoElement);

  return fighterElement;
}

export function createFighterImage(fighter: IFighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createChooseFighterTitle() {
  const element = createElement({
    tagName: 'p',
    className: 'fighter-preview___choose-fighter-title',
  });
  appendTextNode('Choose your opponent', element);
  return element;
}

/**
 * Render fighter stats
 * @param {object} fighter
 */
function createFighterInfo(fighter: IFighterDetails) {
  const { health, attack, defense } = fighter;
  const listElement = createList([
    `Health: ${health}`,
    `Attack: ${attack}`,
    `Defense: ${defense}`,
  ]);

  const { name } = fighter;
  const nameElement = createElement({ tagName: 'h1' });
  appendTextNode(name, nameElement);

  const containerElement = createElement({
    tagName: 'section',
    className: 'fighter-preview__info',
  });

  containerElement.append(nameElement);
  containerElement.append(listElement);
  return containerElement;
}
