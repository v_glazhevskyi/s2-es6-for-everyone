type TCreatingOptions = {
  tagName: string;
  className?: string;
  attributes?: {
    [index: string]: string;
  };
}

export function createElement(options: TCreatingOptions): HTMLElement {
  const { tagName, className, attributes } = options;

  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
  }

  return element;
}

/**
 * Create text node and append it as child to element
 * @param {string} text
 * @param {object} element Node
 */
export function appendTextNode(text: string, element: HTMLElement): void {
  const node = document.createTextNode(text);
  element.appendChild(node);
}

export function createListItem(text: string): HTMLElement {
  const element = createElement({ tagName: 'li' });
  appendTextNode(text, element);
  return element;
}

export function createList(values: string[]): HTMLElement {
  const listElement = createElement({ tagName: 'ul' });
  const append = (item: HTMLElement) => listElement.append(item);
  values.map(createListItem).forEach(append);
  return listElement;
}
