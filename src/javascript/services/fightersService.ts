import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      return <IFighter[]> await callApi(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      return <IFighterDetails> await callApi(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
