type Url = string;

declare module '*.png' {
  const content: Url;
  export default content;
}