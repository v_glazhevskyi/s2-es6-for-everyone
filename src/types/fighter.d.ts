interface IFighter {
  _id: string;
  name: string;
  source: string;
};

interface IFighterDetails {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
};

interface IFighterSelector {
  (event: Event, fighterId: string): Promise<void>;
};

type FighterCache = Map<string, IFighterDetails>;

type FighterResponse = IFighter[] | IFighterDetails;

type MaybeFighter = IFighterDetails | null | undefined;

type FighterPair = [IFighterDetails?, IFighterDetails?];

interface IFakeApiCallFn {
  (endpoint: string): Promise<FighterResponse>;
}