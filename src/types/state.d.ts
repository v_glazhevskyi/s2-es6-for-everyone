type StateProp<V> = WeakMap<IFighter, V>;

interface IState {
  hasBlock: StateProp<boolean>;
  currentHealth: StateProp<number>;
  totalHealth: StateProp<number>;
  indicatorElement: StateProp<HTMLElement>;
  lastCritTime: StateProp<Date>;
};
